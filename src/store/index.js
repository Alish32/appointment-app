import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    appointments: [
      {
        id: 1,
        name: 'Appointment 1',
        date: '28-08-2020',
        note: 'Some notes 1',
        isCompleted: false
      },
      {
        id: 2,
        name: 'Appointment 2',
        date: '28-09-2020',
        note: 'Some notes 2',
        isCompleted: false
      },
      {
        id: 3,
        name: 'Appointment 3',
        date: '28-10-2020',
        note: 'Some notes 3',
        isCompleted: false
      },
      {
        id: 4,
        name: 'Appointment 4',
        date: '21-04-2020',
        note: 'Some notes 4',
        isCompleted: false
      },
    ],
    latestAppointment: {},
    confirmationDialogBoxIsOpen: false,
  },
  mutations: {
    setAppointment(state, appointment) {
      state.appointments = appointment
    },
    setLatestAppointment(state, latestAppointment) {
      state.latestAppointment = latestAppointment
    },
    setConfirmationDialogBoxIsOpen(state, confirmationDialogBoxIsOpen) {
      state.confirmationDialogBoxIsOpen = confirmationDialogBoxIsOpen
    },
  },
  actions: {
    updateAppointments({ commit }, appointment) {
      commit('setAppointment', appointment)
    },
    updateLatestAppointment({ commit }, latestAppointment) {
      commit('setLatestAppointment', latestAppointment)
    },
    updateConfirmationDialogBoxIsOpen({ commit }, confirmationDialogBoxIsOpen) {
      commit('setConfirmationDialogBoxIsOpen', confirmationDialogBoxIsOpen)
    },
  },
  modules: {
  }
})
