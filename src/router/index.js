import Vue from 'vue'
import VueRouter from 'vue-router'
import MakeAppointments from '../views/MakeAppointments.vue'
import Info from '../components/Info'
import Appointments from '../views/Appointments'
import Confirmation from '../components/Confirmation'

Vue.use(VueRouter)

const routes = [
  {
    path: '/', component: Appointments
  },
  {
    path: '/make-appointments', component: MakeAppointments,
    children: [
      { path: '/make-appointments/info', component: Info },
      { path: '/make-appointments/confirmation', component: Confirmation }
    ]
  },
]

const router = new VueRouter({
  routes
})

export default router
